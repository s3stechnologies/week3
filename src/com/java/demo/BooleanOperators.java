package com.java.demo;

public class BooleanOperators {

	
	//&& , ||, & , |, ^ , !
	
	// short circuit &&, || 
	
	public static void main(String[] args) {
		
		int a = 5; 
		int b = 5; 
		
		String n = "abc";
		String nm = "abcd";
	
	
		boolean isStringEqual = (n == nm);
		
		//System.out.println(isStringEqual);
		
		boolean isNotEqual = (a != b); 
		//System.out.println(isNotEqual);
		
		
		// &&, it should satisfy both conditions.. if one is true and another is false 
		//it will say it is false
		boolean isEqual = ((a != b && a == b ) && (a==b && a==b)); 
		
		// OR || works with any of the condition satisfied
		boolean isOrEqual = ((a!= b || a == b ) || a > b );
		
		boolean isExclusiveOr = (a != b  ^ a == b);
		
		
		boolean isTrue = (!true);
		
		System.out.println(isExclusiveOr);
		
		
		a = a+1;
		
		a += 1;
		
		a = a -1;
		a -= 1;
		
		a = a/5;
		a /= 5;
		
		
		
	}

}
