package com.java.demo;


// primitive are passed by value(copy)
public class PassByValue {

	
	static int printNumber(int num) {
		
		num = num + 1 ;
		
		System.out.println("number inside the method: " + num);
		
		return num;
		
	}
	
	
	public static void main(String[] args) {
		
		int x = 5;
		
		System.out.println("Number before passing to the method = " + x);
		
		 printNumber(x);
		
		System.out.println("number after passing to the method = " +x);
		
		
	}

}
