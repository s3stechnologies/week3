package com.java.demo;

public class ComparisonOperators {

//	> , <, >= , <=, ==, !=;
	
	
	public static void main(String[] args) {
		
		int a = 5; 
		int b = 6; 
		
		String n = "abc";
		String nm = "abcd";
	
	
		boolean isStringEqual = (n == nm);
		
		//System.out.println(isStringEqual);
		
		boolean isNotEqual = (a != b); 
		System.out.println(isNotEqual);
		
		boolean isEqual = (a == b); 
		
		System.out.println(isEqual);
	}

}
