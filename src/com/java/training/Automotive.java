package com.java.training;

public class Automotive {
 
		private String makeModel = "Mini Cooper";   // states // instance variable
		
		
		public String getMakeModel() {
			return makeModel;
		}

		public void setMakeModel(String makeModel) {
			
			this.makeModel = makeModel;
		}

				
		String roadReader = "Read traffic lines"; // states
	
	
	// behaviour 	
	void start(String makeModel) { 
	
		System.out.println("I’m starting my " +makeModel);
	  
	}

	void laneSwitch() { // methods
	System.out.println("Lane switching indicator" + roadReader);
	}
	
	
	void firstName() {
		
		System.out.println("Biwash ");
		
	}
	
	void lastName(String name) {
		// code done here
	}
	
	
	
	
}
