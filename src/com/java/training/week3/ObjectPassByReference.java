package com.java.training.week3;

public class ObjectPassByReference {
	
	
	static void passByReference(StringBuilder firstName) {
		
		firstName = firstName.append(" Bohara");
		
		System.out.println(" While we in the method :" +firstName );
		
	}

	public static void main(String[] args) {
		
		StringBuilder stringBuilder = new StringBuilder("Biwash");
		
		System.out.println("before passing it to the method: "  + stringBuilder);
		
		passByReference(stringBuilder);
		
		System.out.println("After passing it to the method parameter: " + stringBuilder);

	}

}
